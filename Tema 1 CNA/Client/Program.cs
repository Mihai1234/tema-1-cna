﻿using Grpc.Net.Client;
using System;
using System.Threading.Tasks;
using Tema_1_CNA;

namespace Client
{                 //Am avut probleme cu localhost si nu am putut sa verific
    class Program //Ce am lasat comentat aici si in GreeterService este in caz ca se doreste afisare la client
    {
        static /*async Task*/ void Main(string[] args)
        {
            var value = Console.ReadLine();
            var input = new HelloRequest { Name = value };
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Greeter.GreeterClient(channel);
           // var reply = await client.SayHelloAsync(input);
            //Console.WriteLine(reply.Message);
            Console.ReadLine();
        }
    }
}
